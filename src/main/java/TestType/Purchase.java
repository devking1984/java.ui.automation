package TestType;
import Pages.*;
import common.Web;

public class Purchase {

    T_Shirts_Page T_Shirts_Page = new T_Shirts_Page();
    Dresses_Page Dresses_Page = new Dresses_Page();
    Shopping_Cart_Page Shopping_Cart_Page = new Shopping_Cart_Page();
    Address_Page Address_Page = new Address_Page();
    Shipping_Page Shipping_Page = new Shipping_Page();
    Payment_Page Payment_Page = new Payment_Page();
    Confirmation_Page Confirmation_Page = new Confirmation_Page();


    public void Execute(Web driver, String Order_Category, String Product) throws InterruptedException {

        if(Order_Category == "TShirts"){
            T_Shirts_Page.Select_T_Shirt(driver, Product);
            T_Shirts_Page.Add_To_Cart(driver);
            T_Shirts_Page.Proceed_To_Checkout(driver);
        }
        if(Order_Category == "Dresses"){
            Dresses_Page.Select_Dress(driver, Product);
            Dresses_Page.Add_To_Cart(driver);
            Dresses_Page.Proceed_To_Checkout(driver);
        }
        Shopping_Cart_Page.Proceed_To_Checkout(driver);
        Address_Page.Proceed_To_Checkout(driver);
        Shipping_Page.Proceed_To_Checkout(driver);
        Payment_Page.Proceed_To_Checkout(driver);
        Confirmation_Page.Proceed_To_Checkout(driver);
    }

}
