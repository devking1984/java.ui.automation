package common;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import java.util.concurrent.TimeUnit;
import java.time.Instant;
import java.util.concurrent.locks.ReentrantLock;

public class Web {

    public long timeout = 60;
    public WebDriver webDriver;
    //ReentrantLock CloseBrowserLock = new ReentrantLock();

    public Web(WebDriver initDriver) {
        this.webDriver = initDriver;
    }

    /// <summary>
    ///  Returns a WebElement
    /// </summary>
    public WebElement GetElement(String xpath) {
        WebElement element = null;
        Instant start_time = Instant.now();
        JavascriptExecutor jsDriver = (JavascriptExecutor) this.webDriver;

        while ((element == null)) {
            try {
                element = this.webDriver.findElement(By.xpath(xpath));
                jsDriver.executeScript("arguments[0].scrollIntoView();", element);
            }
            catch (Exception e) {
                if ((Instant.now().isAfter(start_time.plusSeconds( this.timeout )))) {
                    this.webDriver.findElement(By.xpath(xpath));
                }

            }

            wait(250);
        }

        return element;
    }

    /// <summary>
    ///  Returns a WebElements Text
    /// </summary>
    public String GetElementText(String xpath) {
        this.webDriver.manage().timeouts().implicitlyWait(this.timeout, TimeUnit.SECONDS);
        String text = this.webDriver.findElement(By.xpath(xpath)).getText();
        this.webDriver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
        return text;
    }

    /// <summary>
    ///  Returns the count of WebElements found on a page
    /// </summary>
    public int GetElementCount(String xpath) {
        this.webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        int count = (int)this.webDriver.findElements(By.xpath(xpath)).stream().count();
        this.webDriver.manage().timeouts().implicitlyWait(this.timeout, TimeUnit.SECONDS);
        return count;
    }

    /// <summary>
    ///  Sets the text into input WebElement
    /// </summary>
    public void SetElementText(String xpath, String text) {
        WebElement element = null;
        Instant start_time = Instant.now();
        JavascriptExecutor jsDriver = (JavascriptExecutor) this.webDriver;

        while ((element == null)) {
            try {
                element = this.webDriver.findElement(By.xpath(xpath));
                jsDriver.executeScript("arguments[0].scrollIntoView();", element);
                String highlightJavascript = "arguments[0].setAttribute('style', 'border: 2px solid yellow;');";
                jsDriver.executeScript(highlightJavascript, element );
                element.clear();
                element.sendKeys(text);
                break;
            }
            catch (Exception e) {
                if ((Instant.now().isAfter(start_time.plusSeconds( this.timeout )))) {
                    this.webDriver.findElement(By.xpath(xpath));
                }

            }
            wait(250);
        }

    }

    /// <summary>
    ///  Clicks a WebElement using Selenium Interactions
    /// </summary>
    public void ClickButton(String xpath) {
        boolean element_clicked = false;
        Instant start_time = Instant.now();
        WebElement element = null;
        Actions button = new Actions(this.webDriver);
        JavascriptExecutor jsDriver = (JavascriptExecutor) this.webDriver;

        while(element == null){
            try
            {
                element = this.webDriver.findElement(By.xpath(xpath));
                jsDriver.executeScript("arguments[0].scrollIntoView();", element);
                String highlightJavascript = "arguments[0].setAttribute('style', 'border: 2px solid yellow;');";
                jsDriver.executeScript(highlightJavascript, element );
                button.click(this.webDriver.findElement(By.xpath(xpath))).build().perform();
                button.release();
            }
            catch (Exception e) {
                if ((Instant.now().isAfter(start_time.plusSeconds( this.timeout )))) {
                    this.webDriver.findElement(By.xpath(xpath));
                }

            }
            wait(250);
        }
    }

    /// <summary>
    ///  Clicks a WebElement using Selenium Interactions. Ignores errors if there any
    /// </summary>
    public void TryToClickButton(String xpath) {
        WebElement element = null;
        Instant start_time = Instant.now();
        Actions button = new Actions(this.webDriver);
        JavascriptExecutor jsDriver = (JavascriptExecutor) this.webDriver;

        try {
            element = this.webDriver.findElement(By.xpath(xpath));
            jsDriver.executeScript("arguments[0].scrollIntoView();", element);
            String highlightJavascript = "arguments[0].setAttribute('style', 'border: 2px solid yellow;');";
            jsDriver.executeScript(highlightJavascript, element );
            button.click(element).build().perform();
            button.release();
        }
        catch (Exception e) {

        }

    }

    /// <summary>
    ///  Clicks a WebElement
    /// </summary>
    public void ClickElement(String xpath) {
        boolean element_clicked = false;
        Instant start_time = Instant.now();
        WebElement element = null;
        JavascriptExecutor jsDriver = (JavascriptExecutor) this.webDriver;

        while(element == null){
            try
            {
                element = this.webDriver.findElement(By.xpath(xpath));
                jsDriver.executeScript("arguments[0].scrollIntoView();", element);
                String highlightJavascript = "arguments[0].setAttribute('style', 'border: 2px solid yellow;');";
                jsDriver.executeScript(highlightJavascript, element );
                this.webDriver.findElement(By.xpath(xpath)).click();
            }
            catch (Exception e) {
                if ((Instant.now().isAfter(start_time.plusSeconds( this.timeout )))) {
                    this.webDriver.findElement(By.xpath(xpath));
                }

            }
            wait(250);
        }
    }

    /// <summary>
    ///  Brings WebElement into focus
    /// </summary>
    public void MoveToElement(String xpath) {
        WebElement element = null;
        try {
            element = this.webDriver.findElement(By.xpath(xpath));
            JavascriptExecutor jsDriver = ((JavascriptExecutor)(this.webDriver));
            jsDriver.executeScript("arguments[0].scrollIntoView();", element);
        }
        catch (Exception e) {

        }

    }

    /// <summary>
    ///  Highlights a WebElement
    /// </summary>
    public void HighlightElement(String xpath) {
        WebElement element = null;
        try {
            element = this.webDriver.findElement(By.xpath(xpath));
            JavascriptExecutor jsDriver = ((JavascriptExecutor)(this.webDriver));
            jsDriver.executeScript("arguments[0].scrollIntoView();", element);
            String highlightJavascript = "arguments[0].setAttribute('style', 'border: 2px solid yellow;');";
            jsDriver.executeScript(highlightJavascript, element );
        }
        catch (Exception e) {

        }

    }

    /// <summary>
    ///  Points cursor over a WebElement
    /// </summary>
    public void MouseOverElement(String xpath) {
        WebElement element = null;
        Instant start_time = Instant.now();

        while ((element == null)) {
            try {
                element = this.webDriver.findElement(By.xpath(xpath));
                Actions action = new Actions(this.webDriver);
                action.moveToElement(element).perform();
                break;
            }
            catch (Exception e) {
                if ((Instant.now().isAfter(start_time.plusSeconds( this.timeout )))) {
                    this.webDriver.findElement(By.xpath(xpath));
                }

            }

            wait(250);

        }

    }

    /// <summary>
    ///  Selects a value from a dropdown list
    /// </summary>
    public void SelectDropdownElement(String xpath, String text) {
        Instant start_time = Instant.now();
        WebElement element = null;
        JavascriptExecutor jsDriver = (JavascriptExecutor) this.webDriver;

        while (element == null) {
            try {
                element = this.webDriver.findElement(By.xpath(xpath));
                Select se = new Select(element);
                se.selectByValue(text);
                String highlightJavascript = "arguments[0].setAttribute('style', 'border: 2px solid yellow;');";
                jsDriver.executeScript(highlightJavascript, element );
                break;
            }
            catch (Exception e) {

                if ((Instant.now().isAfter(start_time.plusSeconds( this.timeout )))) {
                    this.webDriver.findElement(By.xpath(xpath));
                }

            }

            wait(250);
        }

    }

    /// <summary>
    ///  Waits for an element to be visible within specified seconds
    /// </summary>
    public boolean ElementIsDisplayed(String xpath, int waitTimeInSeconds) {
        boolean ElementIsDisplayed = false;
        Instant start_time = Instant.now();
        WebElement element = null;

        while (element == null) {
            try {
                element = this.webDriver.findElement(By.xpath(xpath));
                ElementIsDisplayed = element.isDisplayed();
                break;
            }
            catch (Exception e) {
                if ((Instant.now().isAfter(start_time.plusSeconds( this.timeout )))) {
                    this.webDriver.findElement(By.xpath(xpath));
                }

            }

            wait(250);

        }

        return ElementIsDisplayed;
    }

    /// <summary>
    ///  Waits for an element to exist  within specified seconds
    /// </summary>
    public boolean ElementExists(String xpath, int waitTimeInSeconds) {
        boolean ElementExists = false;
        this.webDriver.manage().timeouts().implicitlyWait(waitTimeInSeconds, TimeUnit.SECONDS);
        if ((this.webDriver.findElements(By.xpath(xpath)).stream().count() > 0)) {
            ElementExists = true;
        }

        this.webDriver.manage().timeouts().implicitlyWait(this.timeout, TimeUnit.SECONDS);
        return ElementExists;
    }

    /// <summary>
    ///  Close Browser
    /// </summary>
    public void CloseBrowser()  {
        try {
            if ((this.webDriver != null)) {
                this.webDriver.close();
                this.webDriver.quit();
            }

        }
        catch (Exception e) {
            // do nothing
        }

    }

    /// <summary>
    ///  Clicks a WebElement. Ignores errors if there any
    /// </summary>
    public void TryToClickElement(String xpath) {
        try {
            this.webDriver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
            WebElement element = this.webDriver.findElement(By.xpath(xpath));
            JavascriptExecutor jsDriver = ((JavascriptExecutor)(this.webDriver));
            String highlightJavascript = "arguments[0].style.cssText = \"\"border-width: 2px; border-style: solid; border-color: yellow\"\";";
            jsDriver.executeScript("arguments[0].scrollIntoView();", element);
            jsDriver.executeScript(highlightJavascript, element );
            this.webDriver.findElement(By.xpath(xpath)).click();
            this.webDriver.manage().timeouts().implicitlyWait(this.timeout, TimeUnit.SECONDS);
        }
        catch (Exception e) {
            // Do nothing
        }

    }

    /// <summary>
    ///  Pauses script
    /// </summary>
    public void wait(int ms)
    {
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }

}
