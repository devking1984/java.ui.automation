package Pages;
import common.Web;

public class T_Shirts_Page {

    public String T_Shirts_Menu_Button = "((//a[text()='T-shirts']))[2]";
    public String Faded_Short_Sleeve_T_Shirts = "//a[contains(text(), 'Faded Short Sleeve T-shirts')]";
    public String Add_To_Cart = "//span[text()='Add to cart']";
    public String Proceed_To_Checkout = "//a[@title='Proceed to checkout']";

    public void Select_T_Shirt(Web driver , String Product){
        driver.ClickElement(T_Shirts_Menu_Button);
        if(Product == "Faded Short Sleeve T-shirts")
        {
            driver.ClickElement(Faded_Short_Sleeve_T_Shirts);
        }
    }

    public void Add_To_Cart(Web driver){
        driver.ClickElement(Add_To_Cart);
    }

    public void Proceed_To_Checkout(Web driver){
        while(driver.ElementExists(Add_To_Cart, 1)){
            driver.ClickButton(Proceed_To_Checkout);
        }
    }

}
