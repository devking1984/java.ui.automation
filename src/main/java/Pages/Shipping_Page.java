package Pages;

import common.Web;

public class Shipping_Page {

    public String Proceed_To_Checkout = "((//span[contains(text(), 'Proceed to checkout')]))[2]";
    public String I_Agree_Checkbox = "//input[@type='checkbox']";

    public void Proceed_To_Checkout(Web driver)
    {
        driver.ClickElement(I_Agree_Checkbox);
        driver.ClickElement(Proceed_To_Checkout);
    }
}
