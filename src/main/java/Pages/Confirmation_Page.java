package Pages;

import common.Web;
import org.testng.Assert;

public class Confirmation_Page {

    public String Status = "//p[contains(@class, 'alert')]";
    public String Expected_Status = "Your order on My Store is complete.";

    public void Proceed_To_Checkout(Web driver)
    {
        String Order_Status = driver.GetElement(Status).getText();
        Assert.assertEquals(Expected_Status, Order_Status);
    }
}
