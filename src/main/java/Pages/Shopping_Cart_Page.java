package Pages;

import common.Web;

public class Shopping_Cart_Page {

    public String Proceed_To_Checkout = "((//a[@title='Proceed to checkout']))[2]";

    public void Proceed_To_Checkout(Web driver)
    {
        driver.ClickElement(Proceed_To_Checkout);
    }
}
