package Pages;

import common.Web;

public class Payment_Page {

    public String Pay_By_Check = "//a[@title='Pay by check.']";
    public String Confirm_My_Order = "//span[text()='I confirm my order']";

    public void Proceed_To_Checkout(Web driver)
    {
        driver.ClickElement(Pay_By_Check);
        driver.ClickElement(Confirm_My_Order);
    }
}
