package Pages;

import common.Web;

public class Address_Page {

    public String Proceed_To_Checkout = "//span[text()='Proceed to checkout']";

    public void Proceed_To_Checkout(Web driver)
    {
        driver.ClickElement(Proceed_To_Checkout);
    }
}
