package Pages;
import common.Web;

public class Dresses_Page {

    public String Dresses_Menu_Button = "((//a[@title='Dresses']))[2]";
    public String Printed_Chiffon_Dress = "//a[@title='Printed Chiffon Dress' and @class='product-name' and @itemprop='url']";
    public String Add_To_Cart = "//button[@name='Submit']";
    public String Proceed_To_Checkout = "//a[@title='Proceed to checkout']";

    public void Select_Dress(Web driver , String Product) throws InterruptedException {
        driver.ClickElement(Dresses_Menu_Button);
        if(Product == "Printed Chiffon Dress")
        {
            driver.ClickButton(Printed_Chiffon_Dress);
        }
    }

    public void Add_To_Cart(Web driver){
        driver.ClickElement(Add_To_Cart);
    }

    public void Proceed_To_Checkout(Web driver) throws InterruptedException {
        driver.ClickButton(Proceed_To_Checkout);

        Thread.sleep(2000);

        while(driver.ElementExists(Add_To_Cart, 1)){
            driver.ClickButton(Proceed_To_Checkout);
        }
    }

}
