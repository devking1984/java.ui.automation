package Pages;
import org.openqa.selenium.WebDriver;
import common.Web;

public class Home_Page {

    public String Sign_in_link = "//a[contains(text(), 'Sign in')]";
    public String Email = "//input[@id='email']";
    public String Password = "//input[@id='passwd']";
    public String Sign_in_button = "//button[@id='SubmitLogin']";

    public void Login(Web driver)
    {
        if(driver.ElementExists(Sign_in_link, 2))
        {
            driver.ClickElement(Sign_in_link);
            driver.SetElementText(Email, "devronking@gmail.com");
            driver.SetElementText(Password, "Password@01");
            driver.ClickElement(Sign_in_button);
        }
    }
}
