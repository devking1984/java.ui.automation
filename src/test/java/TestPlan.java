import Pages.Home_Page;
import common.Browser;
import common.Web;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import TestType.*;

public class TestPlan {

    public Web driver;
    Home_Page Home_Page = new Home_Page();
    Purchase Purchase = new Purchase();

    @BeforeSuite
    public void setUp() {
        WebDriver init_driver = new Browser().Launch();
        init_driver.get("http://automationpractice.com/index.php");
        driver = new Web(init_driver);
        Home_Page.Login(driver);
    }

    //READ INPUT PARAMETERS
    @DataProvider(name = "input_data")
    public static Object[][] data() {
        return new Object[][] {
                { "Purchase", "TShirts", "Faded Short Sleeve T-shirts" },
                { "Purchase", "Dresses", "Printed Chiffon Dress" }};
    }

    //RUN SCENARIOS WITH PARAMETERIZED DATA
    @Test(dataProvider = "input_data")
    public void test(String Test_Type, String Order_Category, String Product) throws InterruptedException {

        if(Test_Type == "Purchase"){
            Purchase.Execute(driver, Order_Category, Product);
        }

    }

    @AfterSuite
    public void tearDown() {
        driver.CloseBrowser();
    }
}
